package ru.geekfactory.web.two.dao.models;

import java.math.BigDecimal;

public class CurrencyModel {
    private Long id;
    private String name;
    private BigDecimal courseToDollar;
    private boolean isDefault;
    private String code;
    private String symbol;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getCourseToDollar() {
        return courseToDollar;
    }

    public void setCourseToDollar(BigDecimal courseToDollar) {
        this.courseToDollar = courseToDollar;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return "CurrencyModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", courseToDollar=" + courseToDollar +
                ", isDefault=" + isDefault +
                ", code='" + code + '\'' +
                ", symbol='" + symbol + '\'' +
                '}';
    }
}
