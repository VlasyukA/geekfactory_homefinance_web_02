package ru.geekfactory.web.two.dao.models;

public enum AccountType {
    DEPOSIT, CREDIT_CARD, DEBIT_CARD, CASH;
}
