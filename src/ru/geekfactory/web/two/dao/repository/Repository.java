package ru.geekfactory.web.two.dao.repository;

import ru.geekfactory.web.two.dao.HomeFinanceDaoException;

import java.util.Collection;

public interface Repository<T, ID> {
    T find(ID id);
    Collection<T> findAll();

    boolean remove(ID id);

    T save(T model);

    T update(T model);
}
