package ru.geekfactory.web.two.dao;

public class HomeFinanceDaoException extends RuntimeException {

    public HomeFinanceDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
