package ru.geekfactory.web.two.dao.repository;

import ru.geekfactory.web.two.dao.HomeFinanceDaoException;
import ru.geekfactory.web.two.dao.models.CurrencyModel;

import java.sql.*;
import java.util.Collection;

public class CurrencyRepository implements Repository<CurrencyModel, Long> {
    private static final String INSERT = "INSERT INTO currency_tbl (name, courseToDollar, isDefault, code, symbol) VALUES (?, ?, ?, ?, ?)";

    @Override
    public CurrencyModel find(Long aLong) {
        return null;
    }

    @Override
    public Collection<CurrencyModel> findAll() {
        return null;
    }

    @Override
    public boolean remove(Long aLong) {
        return false;
    }

    @Override
    public CurrencyModel save(CurrencyModel model) {
        try {
            Connection connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/home_finance?useSSL=false&serverTimezone=UTC", "root", "270691");
            connection.setAutoCommit(false);

            PreparedStatement statement = connection.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, model.getName());
            statement.setBigDecimal(2, model.getCourseToDollar());
            statement.setBoolean(3, model.isDefault());
            statement.setString(4, model.getCode());
            statement.setString(5, model.getSymbol());

            statement.executeUpdate();

            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                model.setId(rs.getLong(1));
            }

            connection.commit();
            return model;
        } catch (SQLException e) {
           throw new HomeFinanceDaoException("error while save currency model", e);
        }
    }

    @Override
    public CurrencyModel update(CurrencyModel model) {
        return null;
    }
}
