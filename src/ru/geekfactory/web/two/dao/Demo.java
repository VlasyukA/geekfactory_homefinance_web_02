package ru.geekfactory.web.two.dao;

import ru.geekfactory.web.two.dao.models.CurrencyModel;
import ru.geekfactory.web.two.dao.repository.CurrencyRepository;

import java.math.BigDecimal;

public class Demo {

    public static void main(String[] args) throws ClassNotFoundException {
        CurrencyModel model = new CurrencyModel();
        model.setName("Доллар");
        model.setCourseToDollar(BigDecimal.ONE);
        model.setDefault(false);
        model.setCode("DOL");
        model.setSymbol("$");

        System.out.println("Before save model = " + model);

        CurrencyRepository currencyRepository = new CurrencyRepository();
        currencyRepository.save(model);

        System.out.println("After save model = " + model);
    }
}
